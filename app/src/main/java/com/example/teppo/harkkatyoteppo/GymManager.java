package com.example.teppo.harkkatyoteppo;

import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class GymManager {

    private ArrayList<Gym> gymArrayList;
    //private ArrayList<String> gymNameArrayList;
    Gym smallGym, bigGym, workoutGym, tennisGym;

    SimpleDateFormat simpleDateFormat;

    private static GymManager gymManager = new GymManager();

    private GymManager() {
        gymArrayList = new ArrayList<Gym>();
        //gymNameArrayList = new ArrayList<String>();
        smallGym = new Gym("Small Gym");
        bigGym = new Gym("Big Gym");
        workoutGym = new Gym("Workout Gym");
        tennisGym = new Gym("Tennis Gym");
        simpleDateFormat = new SimpleDateFormat("hh:mm");

        gymArrayList.add(smallGym);
        gymArrayList.add(bigGym);
        gymArrayList.add(workoutGym);
        gymArrayList.add(tennisGym);

        //gymArrayList.add(new Gym("Sali1"));
        //gymArrayList.add(new Gym("Sali2"));
        //gymArrayList.add(new Gym("Sali3"));

    }


    public static GymManager getInstance() {
        return gymManager;
    }

    /*public void addGymToGymNameArrayList(String gym) {
        gymNameArrayList.add(new String(gym));
    }*/

    public void readXML(InputStream inStr) {
        try {
            Context c = null;
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            //String urlString1 = "MIKÄ TÄHÄN TULEE?";



            InputSource inSrc = new InputSource(inStr );
            Document doc = builder.parse(inSrc);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("Reservation");

            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;





                    String gymname = element.getElementsByTagName("gymname").item(0).getTextContent();
                    String from = element.getElementsByTagName("from").item(0).getTextContent();
                    String to = element.getElementsByTagName("to").item(0).getTextContent();
                    String date = element.getElementsByTagName("date").item(0).getTextContent();
                    String person = element.getElementsByTagName("person").item(0).getTextContent();
                    String reason = element.getElementsByTagName("reason").item(0).getTextContent();

                    System.out.println("TÄSSÄ TÄMÄ toinen sitten: " + gymname);
                    //addGymToGymNameArrayList(gymname);


                    if (gymname.equals("Small Gym") == true) {
                        smallGym.addReservation(gymname, from, to, date, person, reason);
                    } else if (gymname.equals("Big Gym") == true) {
                        bigGym.addReservation(gymname, from, to, date, person, reason);
                    } else if (gymname.equals("Workout Gym") == true) {
                        workoutGym.addReservation(gymname, from, to, date, person, reason);
                    } else if (gymname.equals("Tennis Gym") == true) {
                        tennisGym.addReservation(gymname, from, to, date, person, reason);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /*public ArrayList<String> returnGymNameArrayList() {
        return gymNameArrayList;
    }*/

    public void printArraylists() {
        for (Gym a : gymArrayList) {
            System.out.println(a.getArrayList() + " tästä vielä nimeä:" + a.getGymName());

        }
    }

    public void printItems() {
        ArrayList<Reservation> arr = smallGym.getArrayList();
        for (Reservation a: arr) {
            System.out.println("\nTässä lähtöajankohta: " + a.getFrom() + "\n Tässä loppuajankohta: " +
                    a.getTo() + "\n Tässä Varaaja" + a.getPerson());
        }
    }

    /*public ArrayList<String> getSmallGymReservationData() {

        ArrayList<String> smallGymItemsArrayList = new ArrayList<String>();
        String data;

        ArrayList<Reservation> arr = smallGym.getArrayList();
        for (Reservation a: arr) {
                data = ( "Gym: " + a.getGym() + "\nReservation time: " +a.getFrom() + "-" + a.getTo()
                + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                "\nReason for reservation: " + a.getReason());
                smallGymItemsArrayList.add(data);
        }
        return smallGymItemsArrayList;
    }

    public ArrayList<String> getBigGymReservationData() {

        ArrayList<String> bigGymItemsArrayList = new ArrayList<String>();
        String data;

        ArrayList<Reservation> arr = bigGym.getArrayList();
        for (Reservation a: arr) {
            data = ( "Gym: " + a.getGym() + "\nReservation time: " +a.getFrom() + "-" + a.getTo()
                    + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                    "\nReason for reservation: " + a.getReason());
            bigGymItemsArrayList.add(data);
        }
        System.out.println(bigGymItemsArrayList);
        return bigGymItemsArrayList;
    }

    public ArrayList<String> getWorkoutGymReservationData() {

        ArrayList<String> workoutGymItemsArrayList = new ArrayList<String>();
        String data;

        ArrayList<Reservation> arr = workoutGym.getArrayList();
        for (Reservation a: arr) {
            data = ( "Gym: " + a.getGym() + "\nReservation time: " +a.getFrom() + "-" + a.getTo()
                    + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                    "\nReason for reservation: " + a.getReason());
            workoutGymItemsArrayList.add(data);
        }
        return workoutGymItemsArrayList;
    }

    public ArrayList<String> getTennisGymReservationData() {

        ArrayList<String> tennisGymItemsArrayList = new ArrayList<String>();
        String data;

        ArrayList<Reservation> arr = tennisGym.getArrayList();
        for (Reservation a: arr) {
            data = ( "Gym: " + a.getGym() + "\nReservation time: " +a.getFrom() + "-" + a.getTo()
                    + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                    "\nReason for reservation: " + a.getReason());
            tennisGymItemsArrayList.add(data);
        }
        return tennisGymItemsArrayList;
    }*/

    //TODO TÄMÄ EDELLÄ OLEVIEN TILALLE
    public ArrayList<String> getGymReservationData(int sel, String date) {

        ArrayList<Reservation> arr = null;
        ArrayList<String> gymItemsArrayList = new ArrayList<String>();
        if (sel == 0) {
            arr = smallGym.getArrayList();
        } else if (sel == 1) {
            arr = bigGym.getArrayList();
        } else if (sel == 2) {
            arr = workoutGym.getArrayList();
        } else if (sel == 3) {
            arr = tennisGym.getArrayList();
        }
        String data;

        for (Reservation a: arr) {
            if (a.getDate().equals(date) == true) {
                data = ("Gym: " + a.getGym() + "\nReservation time: " + a.getFrom() + "-" + a.getTo()
                        + "\nReservation date: " + a.getDate() + "\nPerson who reservated: " + a.getPerson() +
                        "\nReason for reservation: " + a.getReason());
                gymItemsArrayList.add(data);
            }
        }
        return gymItemsArrayList;
    }


    //TÄMÄ ALLA OLEVAT

    public ArrayList<Reservation> getGymArrayList(int index) {
        ArrayList<Reservation> arr = gymArrayList.get(index).getArrayList();
        return arr;
    }

    /*public ArrayList<Reservation> getSmallGymArrayList() {
        ArrayList<Reservation> arr = smallGym.getArrayList();
        return arr;
    }

    public ArrayList<Reservation> getBigGymArrayList() {
        ArrayList<Reservation> arr = bigGym.getArrayList();
        return arr;
    }

    public ArrayList<Reservation> getWorkoutGymArrayList() {
        ArrayList<Reservation> arr = workoutGym.getArrayList();
        return arr;
    }

    public ArrayList<Reservation> getTennisGymArrayList() {
        ArrayList<Reservation> arr = tennisGym.getArrayList();
        return arr;
    }*/

    public boolean addReservationAdder(String gymi, String starttitime, String enditime,
                                         String datee, String persoon, String reasoon){

        //TODO tutki TÄSSÄ VÄLISSÄ onko uudella varauksella päällekkäisyyttä vanhjoen varausten kanssa!!

        if (gymi.equals("Small Gym") == true) {

            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                System.out.println("Varaaminen onnistuu");
                smallGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                System.out.println("Ei onnistu varaaminen");
                return false;
            }
        } else if (gymi.equals("Big Gym") == true) {

            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                System.out.println("Varaaminen onnistuu");
                bigGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                System.out.println("Ei onnistu varaaminen");
                return false;
            }

        } else if (gymi.equals("Workout Gym") == true) {
            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                System.out.println("Varaaminen onnistuu");
                workoutGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                System.out.println("Ei onnistu varaaminen");
                return false;
            }
        } else if (gymi.equals("Tennis Gym") == true) {
            if (checkOtherReservations(gymi, starttitime, enditime, datee) == true) {
                System.out.println("Varaaminen onnistuu");
                tennisGym.addReservation(gymi, starttitime, enditime, datee, persoon, reasoon);
                return true;
            } else {
                System.out.println("Ei onnistu varaaminen");
                return false;
            }
        }
        return false;
    }

    public boolean checkOtherReservations(String gymname, String startTime, String endTime, String date) {



        ArrayList<Reservation> arr = null;

        if (gymname.equals("Small Gym") == true) {
            arr = smallGym.getArrayList();
        } else if (gymname.equals("Big Gym") == true) {
            arr = bigGym.getArrayList();
        } else if (gymname.equals("Workout Gym") == true) {
            arr = workoutGym.getArrayList();
        } else if (gymname.equals("Tennis Gym") == true) {
            arr = tennisGym.getArrayList();
        }

        //If there is no reservations in the gym at all, we can make reservation anyways
        if (arr.isEmpty() == true) {
            return true;
        }

        Date compareStartingTime = null, compareEndingTime= null, startingTime = null, endingTime = null;

        for (Reservation a: arr) {
            if (a.getDate().equals(date) == true) {
                //Huomataan, että päivämäärät ovat samat joten tarkistetaan kellonajan päällekkäisyys

                try {
                    compareStartingTime = simpleDateFormat.parse(a.getFrom());
                    compareEndingTime = simpleDateFormat.parse(a.getTo());
                    startingTime = simpleDateFormat.parse(startTime);
                    endingTime = simpleDateFormat.parse(endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                System.out.println("Tässä aiemmin varattu alkuaika: " + compareStartingTime);
                System.out.println("Tässä varattava alkuaika: " + startingTime);
                System.out.println("Tässä aiemmin varattu lopetusaika: " + compareEndingTime);
                System.out.println("Tässä varattava lopetusaika: " + endingTime);

                System.out.println("Tässä aiemmin varattu alkuaika: " + compareStartingTime.getTime());
                System.out.println("Tässä varattava alkuaika: " + startingTime.getTime());
                System.out.println("Tässä aiemmin varattu lopetusaika: " + compareEndingTime.getTime());
                System.out.println("Tässä varattava lopetusaika: " + endingTime.getTime());

                if (((startingTime.getTime() > compareEndingTime.getTime()) ||
                        (endingTime.getTime() < compareStartingTime.getTime())) != true) {

                    System.out.println("KATSO MYÖS TÄMÄ");
                    return false;
                }
            } else {
                System.out.println("Tämä varaus ei ainakaan tällä päivämäärällä");
            }
        }
        System.out.println("KATSO TÄMÄ");
        return true;
    }

    public void deleteReservation(int sel1, int sel2, String date) {

        ArrayList<Reservation> arrays = null;
        int index = -1;
        int helpingIndex = -1;
        int wantedIndex = 0;

        if (sel1 == 0) {
            arrays = smallGym.getArrayList();
        } else if (sel1 == 1) {
            arrays = bigGym.getArrayList();
        } else if (sel1 == 2) {
            arrays = workoutGym.getArrayList();
        } else if (sel1 == 3) {
            arrays = tennisGym.getArrayList();
        }

        for (Reservation a: arrays) {
            helpingIndex ++;
            if (a.getDate().equals(date) == true) {
                index ++;
                if (index == sel2) {
                    System.out.println("Poistetaan tämä: " + arrays.get(wantedIndex) + "ja wantedindex oli: " + wantedIndex);
                    wantedIndex = helpingIndex;
                    //arrays.remove(wantedIndex);
                }
            }
            //wantedIndex++;
            //System.out.println("TÖTTÖRÖÖ");
        }

        gymArrayList.get(sel1).deleteReservation(wantedIndex);
        //smallGym.deleteReservation(wantedIndex);
    }



}
