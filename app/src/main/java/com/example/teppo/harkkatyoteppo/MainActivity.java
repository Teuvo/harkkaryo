package com.example.teppo.harkkatyoteppo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMuxer;
import android.media.MediaRecorder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class MainActivity extends AppCompatActivity {

    Context context = null;
    private ArrayAdapter mAdapter, mAdapter2, mAdapter3;
    GymManager gymManager = GymManager.getInstance();
    XMLWriter xmlWriter = new XMLWriter();
    CSVWriter csvWriter = new CSVWriter();
    Gym gym = new Gym("janne");
    InputStream inStr;
    Spinner spinner, spinnerForOwnReservations;
    ArrayList<String> gymData;
    ListView listView;

    // ########## KÄYTTÖTARKOITUS, VARJAA, AIKA #######
    EditText kaytto;
    EditText varaaja;
    public String purpose = "";
    public String reservator = "";


    EditText time1 = null;
    EditText time2 = null;
    private String timeStart = "00:00";
    private String timeEnd = "23:59";
// ################################################


    // ######PVM VALITSIN ###################
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener; // HOX
    private static final String TAG = "MainActivity";
    public String date;
    // ###################################

    // #################### TEPON 14.12. aamun setit
    private TextView announcer;
    // ##########################

    // ########AJAN VALITSIN 1 #################
    private TextView timelist1;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener; // HOX
    private static final String TAG2 = "MainActivity";
    //###################################################

    // ########AJAN VALITSIN 2 #################
    private TextView timelist2;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener2; // HOX
    private static final String TAG3 = "MainActivity";
    //###################################################



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Tämä ottaa nykyisen päivämäärän
        // ############ PVM ######################
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        date = day + ":" + month + ":" + year; //päivämäärä tallentuu tähän
        String[] date1parts = date.split(":");
        String day1 = date1parts[0];
        String month1 = date1parts[1];
        String year1 = date1parts[2];

        if (day1.length() == 1) {
            day1 = "0" + day1;

        }
        if (month1.length() == 1) {
            month1 = "0" + month1;
        }

        date =  day1+"."+month1+"."+year1;

        // ##########################################



        spinner = findViewById(R.id.spinner);
        spinnerForOwnReservations = findViewById(R.id.spinnerForOwnReservations);

        // TÄMÄ OVERRIDEN SISÄÄN

        // ########### KÄYTTÄJÄ JA KÄYTTÖTARKOITUS

        kaytto = (EditText) findViewById(R.id.tarkoitus);
        varaaja = (EditText) findViewById(R.id.user);

        //############################################

        //######### TIME SELECTION ####################
        //time1=(EditText)findViewById(R.id.time1);
        //time2=(EditText)findViewById(R.id.time2);
        //##############################################


        // ############ PVM VALITSIN ############################

        mDisplayDate = (TextView) findViewById(R.id.tvDate);
        listView = (ListView) findViewById(R.id.listView);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(
                        MainActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                date = day + ":" + month + ":" + year; //päivämäärä tallentuu tähän

                String[] date2parts = date.split(":");
                String day2 = date2parts[0];
                String month2 = date2parts[1];
                String year2 = date2parts[2];

                if (day2.length() == 1) {
                    day2 = "0" + day2;

                }
                if (month2.length() == 1) {
                    month2 = "0" + month2;
                }

                date =  day2+"."+month2+"."+year2;
                mDisplayDate.setText(date);

            }
        };

        //## tähän loppuuu


        // ############### START TIME SELECTION ############################
        timelist1 = (TextView) findViewById(R.id.time1);


        timelist1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal2 = Calendar.getInstance();
                int hour = cal2.get(Calendar.HOUR);
                int minute = cal2.get(Calendar.MINUTE);

                TimePickerDialog timeDialog = new TimePickerDialog(
                        MainActivity.this,
                        android.R.style.Widget_Holo_Light,
                        mTimeSetListener,
                        hour,minute,true);
                timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog.show();
            }
        });
        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //month = month + 1;
                Log.d(TAG2, "onTimeSet: hh/mm: " + hourOfDay + "/" + minute);

                timeStart = hourOfDay+":"+minute; //päivämäärä tallentuu tähän

                timeStart = hourOfDay+":"+minute;
                String[] sparts = timeStart.split(":");
                String stestTime = sparts[0];
                String slastPart = sparts[1];

                if (stestTime.length() == 1) {
                    stestTime = "0" + stestTime;
                }
                if (slastPart.length() == 1) {
                    slastPart = "0" + slastPart;
                }

                timeStart = stestTime + ":" +slastPart;

                timelist1.setText(timeStart);

                System.out.println(timeStart);
            }
        };
        System.out.println(timeStart);

        // ###########################################################

        // ############### END TIME SELECTION ############################
        timelist2 = (TextView) findViewById(R.id.time2);


        timelist2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal3 = Calendar.getInstance();
                int hour2 = cal3.get(Calendar.HOUR);
                int minute2 = cal3.get(Calendar.MINUTE);

                TimePickerDialog timeDialog2 = new TimePickerDialog(
                        MainActivity.this,
                        android.R.style.Widget_Holo_DropDownItem_Spinner,
                        mTimeSetListener2,
                        hour2,minute2,true);
                timeDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog2.show();
            }
        });
        mTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay2, int minute2) {
                //month = month + 1;
                Log.d(TAG3, "onTimeSet: hh:mm: " + hourOfDay2 + ":" + minute2);

                timeEnd = hourOfDay2+":"+minute2;
                String[] eparts = timeEnd.split(":");
                String etestTime = eparts[0];
                String elastPart = eparts[1];

                if (etestTime.length() == 1) {
                    etestTime = "0" + etestTime;

                }
                if (elastPart.length() == 1) {
                    elastPart = "0" + elastPart;
                }

                timeEnd = etestTime +":"+elastPart;


                timelist2.setText(timeEnd);

                System.out.println(timeEnd);
            }
        };





        System.out.println(timeEnd);


        // ###############################################


        //##################3 tepon 14.12 aamun setit
        announcer = (TextView) findViewById(R.id.announcer);
        // #############33

        ArrayList<String> gymNameArrayList = new ArrayList<String>();
        gymData = new ArrayList<String>();
        listView = findViewById(R.id.listView);





        context = MainActivity.this;
        /*try {
            inStr = context.openFileInput("gymXML.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        gym.firstXML(inStr);
        System.out.println("FIRST XML TAKANA");*/


        try {
            inStr = context.openFileInput("gymXML.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        gymManager.readXML(inStr);

        //gymNameArrayList = gymManager.returnGymNameArrayList();


        gymNameArrayList.add("Small Gym");
        gymNameArrayList.add("Big Gym");
        gymNameArrayList.add("Workout Gym");
        gymNameArrayList.add("Tennis Gym");

        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,gymNameArrayList);
        spinner.setAdapter(mAdapter);

        //gym.printArrayList();

        gymManager.printArraylists();

        //gymManager.printSmallGymItems();

        //gymData = gymManager.getGym1ReservationData();


        //mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, gymData);
        //listView.setAdapter(mAdapter2);

        //TODO ÄLÄ POISTA TÄTÄ TÄMÄ ON TÄRKEÄ
        /*try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("gymXML.txt", Context.MODE_PRIVATE));

            ows.write("<Reservations>\n" +
                    "  <Reservation>\n" +
                    "      <gymname>Small Gym</gymname>\n" +
                    "      <from>15:00</from>\n" +
                    "      <to>16:00</to>\n" +
                    "      <date>13.12.2018</date>\n" +
                    "      <person>Matti</person>\n" +
                    "      <reason>PingPong</reason>\n" +
                    "  </Reservation>\n" +
                    "  <Reservation>\n" +
                    "      <gymname>Big Gym</gymname>\n" +
                    "      <from>17:00</from>\n" +
                    "      <to>18:00</to>\n" +
                    "      <date>15.12.2018</date>\n" +
                    "      <person>Seppo</person>\n" +
                    "      <reason>Basketball</reason>\n" +
                    "  </Reservation>\n" +
                    "  <Reservation>\n" +
                    "      <gymname>Big Gym</gymname>\n" +
                    "      <from>18:30</from>\n" +
                    "      <to>19:30</to>\n" +
                    "      <date>15.12.2018</date>\n" +
                    "      <person>Kalle</person>\n" +
                    "      <reason>Sähly</reason>\n" +
                    "  </Reservation>\n" +
                    "  <Reservation>\n" +
                    "      <gymname>Workout Gym</gymname>\n" +
                    "      <from>17:00</from>\n" +
                    "      <to>18:00</to>\n" +
                    "      <date>15.12.2018</date>\n" +
                    "      <person>Seppo</person>\n" +
                    "      <reason>Pilates</reason>\n" +
                    "  </Reservation>\n" +
                    "  <Reservation>\n" +
                    "      <gymname>Tennis Gym</gymname>\n" +
                    "      <from>15:00</from>\n" +
                    "      <to>16:00</to>\n" +
                    "      <date>13.12.2018</date>\n" +
                    "      <person>Matti</person>\n" +
                    "      <reason>Tennis</reason>\n" +
                    "  </Reservation>\n" +
                    "  <Reservation>\n" +
                    "      <gymname>Tennis Gym</gymname>\n" +
                    "      <from>15:00</from>\n" +
                    "      <to>17:00</to>\n" +
                    "      <date>14.12.2018</date>\n" +
                    "      <person>Markoboy</person>\n" +
                    "      <reason>Tennisplaying</reason>\n" +
                    "  </Reservation>\n" +
                    "</Reservations>");
            System.out.println("Kirjoitettud");
            ows.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Kirjoitettu");
        }*/


    }

    public void pushSearch(View v) {

        // TÄMÄ public void pushSearch(View v) sisään

        // ######## TIME SELECTION ################
        /*if (time1.length() != 0) {
            timeStart= time1.getText().toString();
        }
        if (time2.length() != 0)  {

            timeEnd= time2.getText().toString();
        }*/

        // AJAN MUOTOILUA

       /* timeStart = timeStart.replace(":", "");
        int intTime1=Integer.parseInt(stringTime1);

        stringTime2 = stringTime2.replace(":", "");
        int intTime2=Integer.parseInt(stringTime2);

        System.out.println(intTime1 + "    "+ intTime2);*/
//###############################################################


// ####### MUUTTUJIA ###########################

        purpose = kaytto.getText().toString();
        reservator = varaaja.getText().toString();

        System.out.println("SALI     "+gymData);
        System.out.println("KÄYTTÖTARKOITUS     "+purpose);
        System.out.println("VARAAJA     "+reservator);
        System.out.println("ALOITUSAIKA     "+timeStart);
        System.out.println("LOPETUSAIKA     "+timeEnd);
        System.out.println("PVM     "+date);

        //###########################################



        int selection = spinner.getSelectedItemPosition();
        System.out.println(selection);

        //VAIHTOEHTO #1

        /*if (selection == 0) {
            gymData = gymManager.getSmallGymReservationData();
        } else if (selection == 1) {
            gymData = gymManager.getBigGymReservationData();
        } else if (selection == 2) {
            gymData = gymManager.getWorkoutGymReservationData();
        } else if (selection == 3) {
            gymData = gymManager.getTennisGymReservationData();
        }*/
        //VAIHTOEHTO #2 (järkevämpi)
        gymData = gymManager.getGymReservationData(selection, date);

        System.out.println(gymData);
        mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, gymData);
        listView.setAdapter(mAdapter2);

        mAdapter3 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, gymData);
        spinnerForOwnReservations.setAdapter(mAdapter3);

    }



    public void close(View v) {

        //TODO ALLA OLEVA KOODI SIIRRETÄÄN xmlwriteriin!!! (TÄMÄ KOODI TOIMII)
        /*try{
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("gymXML.txt", Context.MODE_PRIVATE));
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("<Reservations>\n");

            ArrayList<Reservation> smallGym = gymManager.getSmallGymArrayList();

            for (int i=0; i<smallGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + smallGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + smallGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + smallGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + smallGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + smallGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + smallGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }

            ArrayList<Reservation> bigGym = gymManager.getBigGymArrayList();

            for (int i=0; i<bigGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + bigGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + bigGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + bigGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + bigGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + bigGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + bigGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }

            ArrayList<Reservation> workoutGym = gymManager.getWorkoutGymArrayList();

            for (int i=0; i<workoutGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + workoutGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + workoutGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + workoutGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + workoutGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + workoutGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + workoutGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }

            ArrayList<Reservation> tennisGym = gymManager.getTennisGymArrayList();

            for (int i=0; i<tennisGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + tennisGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + tennisGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + tennisGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + tennisGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + tennisGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + tennisGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }


            stringBuilder.append("</Reservations>");

            String xmlText = stringBuilder.toString();
            ows.write(xmlText);
            ows.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Kirjoitettu");
        }*/

        xmlWriter.writeXML(context);
        csvWriter.wCSV(context);

        System.exit(0);
    }

    public void pushAdd(View v){
        // TÄMÄ public void pushSearch(View v) sisään

        // ######## TIME SELECTION ################
        /*if (time1.length() != 0) {
            timeStart= time1.getText().toString();
        }
        if (time2.length() != 0)  {

            timeEnd= time2.getText().toString();
        }*/

        // AJAN MUOTOILUA

       /* timeStart = timeStart.replace(":", "");
        int intTime1=Integer.parseInt(stringTime1);

        stringTime2 = stringTime2.replace(":", "");
        int intTime2=Integer.parseInt(stringTime2);

        System.out.println(intTime1 + "     "+ intTime2);*/
//###############################################################


// ####### MUUTTUJIA ###########################

        purpose = kaytto.getText().toString();
        reservator = varaaja.getText().toString();

        System.out.println("KÄYTTÖTARKOITUS     "+purpose);
        System.out.println("VARAAJA     "+reservator);
        System.out.println("ALOITUSAIKA     "+timeStart);
        System.out.println("LOPETUSAIKA     "+timeEnd);
        System.out.println("PVM     "+date);

        //###########################################

        int selection = spinner.getSelectedItemPosition();
        String gymname = "";

        if (selection == 0) {
            gymname = "Small Gym";
        } else if (selection == 1) {
            gymname = "Big Gym";
        } else if (selection == 2) {
            gymname = "Workout Gym";
        } else if (selection == 3) {
            gymname = "Tennis Gym";
        }

        if (purpose.equals("") == true) {
            announcer.setText("Please add purpose!");
        } else if ((gymManager.addReservationAdder(gymname,timeStart, timeEnd, date, reservator, purpose)) == false) {
            announcer.setText("You can't reserve that!");
        } else {
            announcer.setText("Reservation made!");
        }


    }

    public void pushCancelReservation (View v) {
        int selection = spinner.getSelectedItemPosition();
        int selection2 = spinnerForOwnReservations.getSelectedItemPosition();

        gymManager.deleteReservation(selection, selection2, date);
    }

    public void changeReservation (View v) {



        int selection = spinner.getSelectedItemPosition();
        int selection2 = spinnerForOwnReservations.getSelectedItemPosition();

        gymManager.deleteReservation(selection, selection2, date);
        pushAdd(v);
    }
}