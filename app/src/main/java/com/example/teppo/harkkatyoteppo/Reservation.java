package com.example.teppo.harkkatyoteppo;

public class Reservation {

    private String gym;
    private String from;
    private String to;
    private String date;
    private String person;
    private String reason;


    public Reservation(String gymm, String startTime, String endTime, String datee, String personn, String reasonn) {
        gym = gymm;
        from = startTime;
        to = endTime;
        date = datee;
        person = personn;
        reason = reasonn;

    }


    public String getGym() { return gym; }

    public String getFrom() {
        return from;
    }

    public String getTo() { return to; }

    public String getDate() { return date; }

    public String getPerson() {
        return person;
    }

    public String getReason() {
        return reason;
    }



}
