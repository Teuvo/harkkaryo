package com.example.teppo.harkkatyoteppo;


import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class XMLWriter {

    GymManager gymManager = GymManager.getInstance();

    public void writeXML(Context context) {
        try{
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("gymXML.txt", Context.MODE_PRIVATE));
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("<Reservations>\n");

            /*
            ArrayList<Reservation> smallGym = gymManager.getSmallGymArrayList();


            for (int i=0; i<smallGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + smallGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + smallGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + smallGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + smallGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + smallGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + smallGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }

            ArrayList<Reservation> bigGym = gymManager.getBigGymArrayList();


            for (int i=0; i<bigGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + bigGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + bigGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + bigGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + bigGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + bigGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + bigGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }

            ArrayList<Reservation> workoutGym = gymManager.getWorkoutGymArrayList();


            for (int i=0; i<workoutGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + workoutGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + workoutGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + workoutGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + workoutGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + workoutGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + workoutGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }

            ArrayList<Reservation> tennisGym = gymManager.getTennisGymArrayList();


            for (int i=0; i<tennisGym.size(); i++){
                stringBuilder.append("  <Reservation>\n");
                stringBuilder.append("      <gymname>" + tennisGym.get(i).getGym() + "</gymname>\n");
                stringBuilder.append("      <from>" + tennisGym.get(i).getFrom() + "</from>\n");
                stringBuilder.append("      <to>" + tennisGym.get(i).getTo() + "</to>\n");
                stringBuilder.append("      <date>" + tennisGym.get(i).getDate() + "</date>\n");
                stringBuilder.append("      <person>" + tennisGym.get(i).getPerson() + "</person>\n");
                stringBuilder.append("      <reason>" + tennisGym.get(i).getReason() + "</reason>\n");
                stringBuilder.append("  </Reservation>\n");
            }*/

            //##########################TÄMÄ KORVAA YLEMMÄN KOMMENTIN#######################
            for (int j = 0; j < 4; j++) {
                ArrayList<Reservation> Gym = gymManager.getGymArrayList(j);

                for (int i=0; i<Gym.size(); i++){
                    stringBuilder.append("  <Reservation>\n");
                    stringBuilder.append("      <gymname>" + Gym.get(i).getGym() + "</gymname>\n");
                    stringBuilder.append("      <from>" + Gym.get(i).getFrom() + "</from>\n");
                    stringBuilder.append("      <to>" + Gym.get(i).getTo() + "</to>\n");
                    stringBuilder.append("      <date>" + Gym.get(i).getDate() + "</date>\n");
                    stringBuilder.append("      <person>" + Gym.get(i).getPerson() + "</person>\n");
                    stringBuilder.append("      <reason>" + Gym.get(i).getReason() + "</reason>\n");
                    stringBuilder.append("  </Reservation>\n");
                }
            }
            //#######################################TÄHÄN LOPPUU###############

            stringBuilder.append("</Reservations>");

            String xmlText = stringBuilder.toString();
            ows.write(xmlText);
            ows.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Kirjoitettu");
        }
    }






}
