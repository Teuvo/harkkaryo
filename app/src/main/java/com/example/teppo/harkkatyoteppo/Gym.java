package com.example.teppo.harkkatyoteppo;

import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Gym {

    private ArrayList<Reservation> reservationArrayList;
    private String gymName;


    //public Gym(String gymm, ArrayList<Reservation> arr) { //TODO tänne pitäisi heittää tieto mikä sali, ja sen perusteella etsiä oikeat tiedot xml:stä, ELI tätä kautta siis kohti firstXML
    //public Gym(String gymm, ArrayList<Reservation> reservations) {
    public Gym(String gymm) {
        gymName = gymm;
        //reservationArrayList = reservations;

        reservationArrayList = new ArrayList<Reservation>();

        /*reservationArrayList.add(new Reservation("15:00",
                "17:00", "12.12.2018", "Matti"));
        reservationArrayList.add(new Reservation("16:00",
                "17:00", "13.12.2018", "Esko"));
        */

    }

    public void addReservation(String gym, String startTime, String endTime, String date, String person, String reason) {
        reservationArrayList.add(new Reservation(gym, startTime, endTime, date, person, reason));
    }

    /*public void firstXML(InputStream inStr) {
        try {
            Context c = null;
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            //String urlString1 = "MIKÄ TÄHÄN TULEE?";



            InputSource inSrc = new InputSource(inStr);
            Document doc = builder.parse(inSrc);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("Gym");
            System.out.println("HOX HOX : " + nList.getLength());




            //TODO tähän iffi


            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    NodeList varaukset = element.getElementsByTagName("Reservation");
                    for (int j = 0; j < varaukset.getLength() ; j++) {
                        System.out.println("VARAUKSET: " + element.getElementsByTagName("from").item(j).getTextContent());

                        addReservation(element.getElementsByTagName("from").item(j).getTextContent(), element.getElementsByTagName("to").item(j).getTextContent(),
                                element.getElementsByTagName("date").item(j).getTextContent(), element.getElementsByTagName("person").item(j).getTextContent());

                    }
                   // System.out.println("TÄSSÄ TÄMÄ: " + element.getElementsByTagName("from").item(i).getTextContent());

                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }*/


    //public ArrayList<Reservation> returnList() { return reservationArrayList; }

    public void printArrayList() {
        for (Reservation a : reservationArrayList) {
            System.out.println("Ja siitä:" + a.getDate() + " " + a.getFrom() + " " + a.getPerson() + " " + a.getTo());
        }
    }

    public ArrayList<Reservation> getArrayList() {
        return reservationArrayList;
    }

    public String getGymName() {
        return gymName;
    }

    public void deleteReservation(int index) {
        System.out.println("Poistetaan tämä varaus: " + reservationArrayList.get(index));
        reservationArrayList.remove(index);
    }




}
